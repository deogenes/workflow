import { registry } from "tsyringe";
import { RabbitMessageQueueProvider } from "../Provider/RabbitMessageQueueProvider";

export const IocInfra = () => {
  @registry([
    { token: "IMessageQueueProvider", useClass: RabbitMessageQueueProvider }
  ])

  class MyRegistryInfra {}
}