import { ISpec, IWorkflowRepository, WorkflowModel } from '../../../workflow.domain';
import { WorkflowProjection } from '../Projection/WorkflowProjection';
import prisma from '../db';

export class WorkflowRepository implements IWorkflowRepository
{
    constructor()
    {
    }

    async Where(spec: ISpec<WorkflowModel>): Promise<WorkflowModel[]> {
        let response = await prisma.workflow.findMany(await spec.GetSpecification());

        return response.map(x => WorkflowProjection.ToModel(x));
    }

    async Find(spec: ISpec<WorkflowModel>): Promise<WorkflowModel | null> {
        let response = await prisma.workflow.findFirst(await spec.GetSpecification());

        if(response == null)
            return null;

        return WorkflowProjection.ToModel(response);
    }

    async Update(spec: ISpec<WorkflowModel>): Promise<void> {
        await prisma.workflow.update(await spec.GetSpecification());
    }
}