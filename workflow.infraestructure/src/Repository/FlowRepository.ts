import { IFlowModel, IFlowRepository, ISpec } from '../../../workflow.domain';
import { FlowSpec } from '../Spec/FlowSpec';
import { FlowProjection } from '../Projection/FlowProjection';
import { UpdateFlowSpec } from '../Spec/UpdateFlowSpec';
import prisma from '../db';

export class FlowRepository implements IFlowRepository
{
    async Delete(spec: ISpec<IFlowModel<any, any, any>>): Promise<void> {
        await prisma.flow.delete(await spec.GetSpecification())
    }

    async Create(spec: ISpec<IFlowModel<any, any, any>>): Promise<void> {
        await prisma.flow.create(await spec.GetSpecification());
    }

    async Update(spec: UpdateFlowSpec): Promise<void>
    {
        let specification = await spec.GetSpecification();
        await prisma.flow.update(specification);

        return Promise.resolve();
    }

    async Where(spec: FlowSpec): Promise<IFlowModel<any, any, any>[]>
    {
        let result = await prisma.flow.findMany(spec.Specification as any);
        return result.map(x => FlowProjection.ToModel(x));
    }

    async Find(spec: FlowSpec): Promise<IFlowModel<any, any, any> | null> {
        let result = await prisma.flow.findFirst(spec.Specification as any);

        if(result == null)
            return null;

        return FlowProjection.ToModel(result);
    }
}