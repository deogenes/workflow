import { FlowDefinitionModel, IFlowDefinitionRepository, ISpec } from '../../../workflow.domain';
import { UpdateFlowDefinitionSpec } from '../Spec/UpdateFlowDefinitionSpec';
import { FlowDefinitionSpec } from '../Spec/FlowDefinitionSpec';
import { FlowDefinitionProjection } from '../Projection/FlowDefinitionProjection';
import prisma from '../db';

export class FlowDefinitionRepository implements IFlowDefinitionRepository
{
    constructor()
    {
    }

    async Create(spec: ISpec<FlowDefinitionModel<any>>): Promise<void> {
        await prisma.flowDefinition.create(await spec.GetSpecification());
    }

    Where(spec: FlowDefinitionSpec): Promise<FlowDefinitionModel<any>[]> {
        throw new Error('Method not implemented.');
    }

    async Update(spec: UpdateFlowDefinitionSpec): Promise<void>
    {
        await prisma.flowDefinition.update(await spec.GetSpecification());

        return Promise.resolve();
    }

    async Find(spec: FlowDefinitionSpec): Promise<FlowDefinitionModel<any> | null> {
        let result = await prisma.flowDefinition.findFirst(await spec.GetSpecification());

        if(result == null)
            return null;

        return FlowDefinitionProjection.ToModel<any>(result);
    }
}