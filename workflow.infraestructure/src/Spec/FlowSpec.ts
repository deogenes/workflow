import { FlowFilter, IFlowModel, ISpec } from '../../../workflow.domain'

type FlowSpecification = {
    include: FlowSpecificationInclude,
    where: any
}

type FlowSpecificationInclude = {
    Definition?: Boolean | { 
        include?: {
            NextFlows: Boolean
        }
    }
}

export class FlowSpec implements ISpec<IFlowModel<any, any, any>>
{
    Specification: FlowSpecification = { include: { }, where: { } }

    GetSpecification(): Promise<any> {
        return Promise.resolve(this.Specification);
    }

    async ByFilter(filter: FlowFilter): Promise<FlowSpec>
    {
        if(filter.Id != undefined)
            await this.ById(filter.Id);

        if(filter.Sequence != undefined)
            await this.BySequence(filter.Sequence);

        if(filter.WorkflowId != undefined)
            await this.ByWorkflowId(filter.WorkflowId);

        return this;
    }

    AddNavigation(arg: FlowSpecificationInclude) : FlowSpec
    {
        this.Specification.include = arg;

        return this;
    }

    async BySequence(arg: string) : Promise<FlowSpec>
    {
        this.Specification.where = Object.assign(this.Specification.where, {
            Sequence: arg
        });

        return this;
    }

    async ByWorkflowId(arg: string) : Promise<FlowSpec>
    {
        this.Specification.where = Object.assign(this.Specification.where, {
            WorkflowId: arg
        });

        return this;
    }

    async ById(id: string) : Promise<FlowSpec>
    {
        this.Specification.where = Object.assign(this.Specification.where, {
            Id: id
        });

        return this;
    }
}