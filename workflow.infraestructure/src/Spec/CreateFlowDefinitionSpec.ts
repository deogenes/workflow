import { IFlowModel, ISpec } from '../../../workflow.domain'

export type CreateFlowDefinitionSpecificationData = {
    Id?: String,
    Properties?: any,
    FlowId?: String
}

type CreateFlowDefinitionSpecification = {
    data: CreateFlowDefinitionSpecificationData
}

export class CreateFlowDefinitionSpec implements ISpec<IFlowModel<any, any, any>>
{
    GetSpecification(): Promise<any> {
        return Promise.resolve(this.Specification);
    }

    Specification: CreateFlowDefinitionSpecification = { data: { } };

    async SetValue(value: CreateFlowDefinitionSpecificationData) : Promise<CreateFlowDefinitionSpec>
    {
        this.Specification.data = value
        
        return this;
    }
}