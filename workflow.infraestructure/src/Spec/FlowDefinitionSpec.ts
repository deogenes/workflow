import { FlowDefinitionFilter, FlowFilter, IFlowModel, ISpec } from '../../../workflow.domain'

type FlowDefinitionSpecification = {
    include: FlowDefinitionSpecificationInclude,
    where: {
        FlowId?: String
    }
}

type FlowDefinitionSpecificationInclude = {
    
}

export class FlowDefinitionSpec implements ISpec<IFlowModel<any, any, any>>
{
    Specification: FlowDefinitionSpecification = { include: { }, where: { } }

    GetSpecification(): Promise<any> {
        return Promise.resolve(this.Specification);
    }

    async ByFilter(filter: FlowDefinitionFilter): Promise<FlowDefinitionSpec>
    {
        if(filter.Id != undefined)
            await this.ById(filter.Id);

        return this;
    }

    AddNavigation(arg: FlowDefinitionSpecificationInclude) : FlowDefinitionSpec
    {
        this.Specification.include = arg;
        return this;
    }

    async ById(id: string) : Promise<FlowDefinitionSpec>
    {
        this.Specification.where.FlowId = id;
        return this;
    }
}