import { IFlowModel, ISpec, UpdateFlowFilter } from '../../../workflow.domain'

type UpdateFlowSpecification = {
    where: {
        Id?: string
        DefinitionId?: string
        WorkflowId?: string
    },
    data: any
}

export class UpdateFlowSpec implements ISpec<IFlowModel<any, any, any>>
{
    async GetSpecification(): Promise<any> {
        return Promise.resolve(this.Specification);
    }

    Specification: UpdateFlowSpecification = { data: {}, where: {} };

    async ByFilter(filter: UpdateFlowFilter): Promise<UpdateFlowSpec>
    {
        if(filter.Id != undefined)
            await this.WhereId(filter.Id);

        if(filter.WorkflowId != undefined)
            await this.WhereWorkflowId(filter.WorkflowId);

        return this;
    }

    async SetValue(value: {
        Id? : string,
        DefinitionId?: string,
        Executed?: boolean,
        QualifiedName?: string,
        Sequence?: number,
        Success?: boolean,
        Tries?: number,
        WorkflowId?: string
    }) : Promise<UpdateFlowSpec>
    {
        this.Specification.data = value;
        
        return this;
    }

    async WhereWorkflowId(arg: string) : Promise<UpdateFlowSpec>
    {
        this.Specification.where.DefinitionId = arg;

        return this;
    }

    async WhereId(id: string) : Promise<UpdateFlowSpec>
    {
        this.Specification.where.Id = id;

        return this;
    }
}