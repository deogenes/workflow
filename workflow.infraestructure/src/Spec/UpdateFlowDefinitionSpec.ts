import { IFlowModel, ISpec, UpdateFlowDefinitionFilter, UpdateFlowFilter } from '../../../workflow.domain'

type UpdateFlowDefinitionSpecification = {
    where: {
        Id?: string
        Properties?: any
        FlowId?: string
    },
    data: any
}

export class UpdateFlowDefinitionSpec implements ISpec<IFlowModel<any, any, any>>
{
    GetSpecification(): Promise<any> {
        return Promise.resolve(this.Specification);
    }

    Specification: UpdateFlowDefinitionSpecification = { data: {}, where: {} };

    async ByFilter(filter: UpdateFlowDefinitionFilter): Promise<UpdateFlowDefinitionSpec>
    {
        if(filter.Id != undefined)
            await this.WhereId(filter.Id);

        if(filter.FlowId != undefined)
            await this.ByFlowId(filter.FlowId);

        return this;
    }

    async SetValue(value: {
        Id? : string,
        FlowId?: string,
        Properties?: any
    }) : Promise<UpdateFlowDefinitionSpec>
    {
        this.Specification.data = Object.assign(this.Specification.data, value);
        
        return this;
    }

    async ByFlowId(id: string) : Promise<UpdateFlowDefinitionSpec>
    {
        this.Specification.where.FlowId = id;

        return this;
    }

    async WhereId(id: string) : Promise<UpdateFlowDefinitionSpec>
    {
        this.Specification.where.Id = id;

        return this;
    }
}