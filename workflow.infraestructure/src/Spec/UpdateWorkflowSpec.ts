import { ISpec, UpdateFlowFilter, WorkflowModel } from '../../../workflow.domain'

type UpdateWorkflowSpecification = {
    where: {
        Id?: string
    },
    data: UpdateWorkflowDataSpecification
}

type UpdateWorkflowDataSpecification = {
    Template?: Object | null
}

export class UpdateWorkflowSpec implements ISpec<WorkflowModel>
{
    async GetSpecification(): Promise<any> {
        return Promise.resolve(this.Specification);
    }

    Specification: UpdateWorkflowSpecification = { data: {}, where: {} };

    async ByFilter(filter: UpdateFlowFilter): Promise<UpdateWorkflowSpec>
    {
        if(filter.Id != undefined)
            await this.WhereId(filter.Id);

        return this;
    }

    async SetValue(value: UpdateWorkflowDataSpecification) : Promise<UpdateWorkflowSpec>
    {
        this.Specification.data = value;
        return this;
    }

    async WhereId(id: string) : Promise<UpdateWorkflowSpec>
    {
        this.Specification.where.Id = id;
        return this;
    }
}