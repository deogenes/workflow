import { ISpec, WorkflowFilter, WorkflowModel } from '../../../workflow.domain'

type WorkflowSpecification = {
    include: FlowSpecificationInclude,
    where: {
        Id?: String
    }
}

type FlowSpecificationInclude = {
}

export class WorkflowSpec implements ISpec<WorkflowModel>
{
    Specification: WorkflowSpecification = { include: { }, where: { } }

    GetSpecification(): Promise<any> {
        return Promise.resolve(this.Specification);
    }

    async ByFilter(filter: WorkflowFilter): Promise<WorkflowSpec>
    {
        if(filter.Id != undefined)
            await this.ById(filter.Id);

        return this;
    }

    AddNavigation(arg: FlowSpecificationInclude) : WorkflowSpec
    {
        this.Specification.include = arg;
        return this;
    }

    async ById(id: string) : Promise<WorkflowSpec>
    {
        this.Specification.where.Id = id;
        return this;
    }
}