import { IFlowModel, ISpec } from '../../../workflow.domain'

type DeleteFlowSpecification = {
    where: {
        Id?: String
    }
}

export class DeleteFlowSpec implements ISpec<IFlowModel<any, any, any>>
{
    Specification: DeleteFlowSpecification = { where: { } }

    GetSpecification(): Promise<any> {
        return Promise.resolve(this.Specification);
    }

    async ById(id: string) : Promise<DeleteFlowSpec>
    {
        this.Specification.where.Id = id;

        return this;
    }
}