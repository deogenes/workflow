import { IFlowModel, ISpec } from '../../../workflow.domain'

export type CreateFlowSpecificationData = {
    Id?: String,
    QualifiedName?: String,
    Sequence?: number,
    WorkflowId?: String
}

type CreateFlowSpecification = {
    data: CreateFlowSpecificationData
}

export class CreateFlowSpec implements ISpec<IFlowModel<any, any, any>>
{
    GetSpecification(): Promise<any> {
        return Promise.resolve(this.Specification);
    }

    Specification: CreateFlowSpecification = { data: { } };

    async SetValue(value: CreateFlowSpecificationData) : Promise<CreateFlowSpec>
    {
        this.Specification.data = value
        
        return this;
    }
}