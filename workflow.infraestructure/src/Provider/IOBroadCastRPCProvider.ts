import { Server } from "socket.io";
import { IBroadCastRPCProvider } from "../../../workflow.domain";
import { injectable } from "tsyringe";

@injectable()
export class IOBroadCastRPCProvider implements IBroadCastRPCProvider
{
    constructor(
        private io: Server
    )
    {
    }
    
    async Call<T>(method: string, ...args: any): Promise<T> {
        return await this.io.serverSideEmitWithAck(method, args) as T;
    }
}