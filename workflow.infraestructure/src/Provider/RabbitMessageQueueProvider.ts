import client, { Channel, Connection, ConsumeMessage, Message } from "amqplib";
import { IMessageQueue, IMessageQueueProvider, MessageQueueEnvelop } from "../../../workflow.domain";
import { inject, injectable } from 'tsyringe';

export class RabbitMessageQueue implements IMessageQueue
{
  constructor(private message: Message, private rabbitConnection: RabbitMessageQueueProvider)
  {
  }

  Content<T>(): Promise<T> {
    let message = this.message.content.toString();
    return Promise.resolve(message as T);
  }

  Reject(): Promise<void> {
    this.rabbitConnection.channel.reject(this.message, true);

    return Promise.resolve();
  }

  Confirm(): Promise<void> {
    this.rabbitConnection.channel.ack(this.message);
    return Promise.resolve();
  }
}

type HandlerCB = (msg: Message) => any;

@injectable()
export class RabbitMessageQueueProvider implements IMessageQueueProvider
{
    connection!: Connection;
    channel!: Channel;
    private connected!: Boolean;

    constructor(
      private rmqUser: string,
      private rmqPass: string,
      private rmqhost: string,
      private port: number
    )
    {
      
    }
    
    async Consume(subject: string, handler: (msg: IMessageQueue) => any): Promise<void> {
        await this.consume(subject, async (msg) => {
          let message = new RabbitMessageQueue(msg, this);
          await handler(message);
        });
    }

    async Publish(subject: string, envelop: MessageQueueEnvelop): Promise<void> {
      await this.sendToQueue(subject, await envelop.Content);
    }

  async connect()
  {
    if (this.connected && this.channel)
      return;
    else
      this.connected = true;

    try
    {
      console.log(`⌛️ Connecting to Rabbit-MQ Server`);
      
      this.connection = await client.connect(
        `amqp://${this.rmqUser}:${this.rmqPass}@${this.rmqhost}:${this.port}`
      );

      console.log(`✅ Rabbit MQ Connection is ready`);

      if(!this.channel)
      {
        console.log(`🛸 Created RabbitMQ Channel successfully`);
        this.channel = await this.connection.createChannel();
        this.channel.prefetch(2);
      }
      
    } catch (error) {
      console.error(error);
      console.error(`Not connected to MQ Server`);
    }
  }

  async sendToQueue(queue: string, message: any)
  {
    try {
      if (!this.channel)
        await this.connect();

      this.channel.sendToQueue(queue, Buffer.from(JSON.stringify(message)));
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  private async consume(queue: string, handleIncomingNotification: HandlerCB) {

    if (!this.channel)
      await this.connect();

    await this.channel.assertQueue(queue, {
      durable: true,
    });

    this.channel.consume(
      queue,
      async (msg: ConsumeMessage | null) => {
        await handleIncomingNotification(msg as Message)
      },
      {
        noAck: false,
      }
    );
  }
}