import { FlowModel, IFlowModel } from "../../../workflow.domain";
import { FlowDefinitionProjection } from "./FlowDefinitionProjection";

export class FlowProjection
{
    public static ToModel<I, O, T>(object: any) : IFlowModel<I, O, T>
    {
        let flowModel = new FlowModel<I, O, T>({
            Executed: object.Executed,
            QualifiedName: object.QualifiedName,
            Id: object.Id,
            Tries: object.Tries,
            Success: object.Success,
            Sequence: object.Sequence,
            WorkflowId: object.WorkflowId
        });

        let definitionArgs = object?.Definition || null;
        flowModel.Definition = FlowDefinitionProjection.ToModel(definitionArgs);

        return flowModel;
    }
}