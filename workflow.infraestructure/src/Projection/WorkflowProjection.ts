import { WorkflowModel } from "../../../workflow.domain";

export class WorkflowProjection
{
    public static ToModel(object: any) : WorkflowModel
    {
        return new WorkflowModel({
            Id: object.Id,
            Template: object.Template
        })
    }
}