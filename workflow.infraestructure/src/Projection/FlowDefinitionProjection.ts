import { FlowDefinitionModel } from "../../../workflow.domain";

export class FlowDefinitionProjection
{
    public static ToModel<T>(object?: any)
    {
        let properties = object?.Properties ?? null;
        let flows : Array<{ NextFlowId: string }> = object?.NextFlows ?? new Array<{ NextFlowId: string }>();

        return new FlowDefinitionModel<T>({
            Properties: properties,
            NextFlow: flows.map(x => x.NextFlowId + '')
        });
    }
}