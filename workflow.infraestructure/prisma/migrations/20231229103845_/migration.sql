-- CreateTable
CREATE TABLE "FlowDefinitionNextFlow" (
    "Id" TEXT NOT NULL,
    "FlowDefinitionId" TEXT NOT NULL,
    "NextFlowId" TEXT NOT NULL,

    CONSTRAINT "FlowDefinitionNextFlow_pkey" PRIMARY KEY ("Id")
);

-- CreateTable
CREATE TABLE "FlowDefinition" (
    "Id" TEXT NOT NULL,
    "Properties" JSONB NOT NULL,
    "FlowId" TEXT NOT NULL,

    CONSTRAINT "FlowDefinition_pkey" PRIMARY KEY ("Id")
);

-- CreateTable
CREATE TABLE "Flow" (
    "Id" TEXT NOT NULL,
    "Tries" INTEGER NOT NULL DEFAULT 0,
    "QualifiedName" TEXT NOT NULL,
    "Executed" BOOLEAN NOT NULL DEFAULT false,
    "Success" BOOLEAN NOT NULL DEFAULT false,
    "Sequence" BIGINT NOT NULL,
    "WorkflowId" TEXT NOT NULL,

    CONSTRAINT "Flow_pkey" PRIMARY KEY ("Id")
);

-- CreateTable
CREATE TABLE "Workflow" (
    "Id" TEXT NOT NULL,

    CONSTRAINT "Workflow_pkey" PRIMARY KEY ("Id")
);

-- CreateIndex
CREATE UNIQUE INDEX "FlowDefinitionNextFlow_NextFlowId_FlowDefinitionId_key" ON "FlowDefinitionNextFlow"("NextFlowId", "FlowDefinitionId");

-- CreateIndex
CREATE UNIQUE INDEX "FlowDefinition_FlowId_key" ON "FlowDefinition"("FlowId");

-- AddForeignKey
ALTER TABLE "FlowDefinitionNextFlow" ADD CONSTRAINT "FlowDefinitionNextFlow_FlowDefinitionId_fkey" FOREIGN KEY ("FlowDefinitionId") REFERENCES "FlowDefinition"("Id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "FlowDefinitionNextFlow" ADD CONSTRAINT "FlowDefinitionNextFlow_NextFlowId_fkey" FOREIGN KEY ("NextFlowId") REFERENCES "Flow"("Id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "FlowDefinition" ADD CONSTRAINT "FlowDefinition_FlowId_fkey" FOREIGN KEY ("FlowId") REFERENCES "Flow"("Id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Flow" ADD CONSTRAINT "Flow_WorkflowId_fkey" FOREIGN KEY ("WorkflowId") REFERENCES "Workflow"("Id") ON DELETE RESTRICT ON UPDATE CASCADE;
