import "reflect-metadata";
import { registry } from "tsyringe";
import { FlowDefinitionRepository, FlowRepository, RabbitMessageQueueProvider, WorkflowRepository } from "../../../workflow.infraestructure";
import { ExecuteStepController } from "../Controller/ExecuteStepController";
import { BatchFlowModel, ConsoleLogFlowModel, ExecuteFlowCommandHandle, GetFlowProcessInfoCommandHandle, GetSequenceInfoCommandHandle, ProcessMap, ScriptFlowModel } from "../../../workflow.application";
import { WorkflowSequencesController } from "../Controller/WorkflowSequencesController";
import { UpdateFlowDefinitionController } from "../Controller/UpdateFlowDefinitionController";
import { WorkflowController } from "../Controller/WorkflowController";
import { UpdateWorkflowController } from "../Controller/UpdateWorkflowController";
import { CreateFlowController } from "../Controller/CreateFlowController";
import { CreateFlowDefinitionController } from "../Controller/CreateFlowDefinitionController";
import { FlowController } from "../Controller/FlowController";
import { DeleteFlowController } from "../Controller/DeleteFlowController";

declare global {
  var rabbitMessageQueueProvider: undefined | RabbitMessageQueueProvider
}

@registry([
  {
    token: "IMessageQueueProvider", useFactory(dependencyContainer) {
      if(globalThis.rabbitMessageQueueProvider == undefined)
        global.rabbitMessageQueueProvider = new RabbitMessageQueueProvider("guest", "guest", "localhost", 5672)

      return global.rabbitMessageQueueProvider
    }
  },
  { token: "IFlowRepository", useClass: FlowRepository  },
  { token: "IFlowDefinitionRepository", useClass: FlowDefinitionRepository  },
  { token: "IWorkflowRepository", useClass: WorkflowRepository },
  { token: "ProcessMap", useClass: ProcessMap },
  { token: "GetFlowProcessInfoCommandHandle", useClass: GetFlowProcessInfoCommandHandle },
  { 
    token: "GetSequenceInfoCommandHandle", useFactory(di) {
      return new GetSequenceInfoCommandHandle(di.resolve("IFlowRepository"), di.resolve("GetFlowProcessInfoCommandHandle"))
    }
  },
  {
    token: "ExecuteFlowCommandHandle", useFactory(di) {
      return new ExecuteFlowCommandHandle(di.resolve("IFlowRepository"))    
    }
  },
  {
    token: "ConsoleLogFlowModel", useFactory(container) {
      return new ConsoleLogFlowModel({})
    }
  },
  {
    token: "BatchFlowModel", useFactory(container) {
      return new BatchFlowModel({})
    }
  },
  {
    token: "ScriptFlowModel", useFactory(container) {
      return new ScriptFlowModel({})
    }
  },
  { token: "ExecuteStepController", useClass: ExecuteStepController },
  { token: "WorkflowSequencesController", useClass: WorkflowSequencesController },
  { token: "UpdateFlowDefinitionController", useClass: UpdateFlowDefinitionController },
  { token: "UpdateWorkflowController", useClass: UpdateWorkflowController },
  { token: "WorkflowController", useClass: WorkflowController },
  { token: "CreateFlowController", useClass: CreateFlowController },
  { token: "CreateFlowDefinitionController", useClass: CreateFlowDefinitionController },
  { token: "FlowController", useClass: FlowController },
  { token: "DeleteFlowController", useClass: DeleteFlowController }
])

class MyRegistry {}