import { inject, injectable } from 'tsyringe';
import { IFlowDefinitionRepository, UpdateFlowDefinitionFilter } from '../../../workflow.domain';
import { UpdateFlowDefinitionSpec } from '../../../workflow.infraestructure';

@injectable()
export class UpdateFlowDefinitionController
{
    constructor(
        @inject("IFlowDefinitionRepository")
        private FlowDefinitionRepository: IFlowDefinitionRepository
    )
    {
    }

    async Handle(envelop: {
        Properties: any
    }, flowId: string)
    {
        let spec = new UpdateFlowDefinitionSpec();

        spec.ByFilter(new UpdateFlowDefinitionFilter({
            FlowId: flowId
        }));

        spec.SetValue(envelop);

        await this.FlowDefinitionRepository.Update(spec);
    }
}