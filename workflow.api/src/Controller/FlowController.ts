import { inject, injectable } from 'tsyringe';
import { IFlowRepository } from '../../../workflow.domain';
import { FlowSpec } from '../../../workflow.infraestructure';
import { FlowViewModel } from '../ViewModel/FlowViewModel';

@injectable()
export class FlowController
{
    constructor(
        @inject("IFlowRepository")
        private FlowRepository: IFlowRepository
    )
    {
    }

    async Handle(id: string)
    {
        let spec = new FlowSpec();
        spec.ByFilter({
            Id: id
        });

        let model = await this.FlowRepository.Find(spec);

        if(!model)
            return null;

        return new FlowViewModel({
            Id: model.Id,
            QualifiedName: model.QualifiedName
        })
    }
}