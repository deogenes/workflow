import { IOBroadCastRPCProvider, Server } from "../../../workflow.infraestructure";
import { FlowInfoViewModel } from "../ViewModel/FlowInfoViewModel";
import { GetSequenceInfoCommandHandle } from "../../../workflow.application";
import { container, inject, injectable } from "tsyringe";
import { GetSequenceInfoCommand } from "../../../workflow.application";

@injectable()
export class WorkflowSequencesController
{
    constructor(
        @inject("GetSequenceInfoCommandHandle")
        private GetSequenceInfoCommandHandle: GetSequenceInfoCommandHandle,
    )
    {
    }

    async Handle(io: Server, workflowId: string, sequenceId: string) : Promise<Array<FlowInfoViewModel>>
    {
        let broadCastProvider = new IOBroadCastRPCProvider(io);

        let command = new GetSequenceInfoCommand({
            SequenceId: sequenceId,
            WorkflowId: workflowId
        })

        let response = await this.GetSequenceInfoCommandHandle.Handle(
            broadCastProvider,
            command
        );

        return response.map(x => new FlowInfoViewModel({
            Active: x.Active,
            Id: x.Id,
            Memory: x.Memory,
            Pid: x.Pid
        }));
    }
}