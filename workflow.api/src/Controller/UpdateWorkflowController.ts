import { container, inject, injectable } from 'tsyringe';
import { IWorkflowRepository, WorkflowFilter } from '../../../workflow.domain';
import { UpdateWorkflowSpec, WorkflowSpec } from '../../../workflow.infraestructure';


@injectable()
export class UpdateWorkflowController
{
    constructor(
        @inject("IWorkflowRepository")
        private WorkflowRepository: IWorkflowRepository
    )
    {
    }

    async Handle(workflowId: string, body: { Template: any })
    {
        let spec = await new WorkflowSpec()
            .ByFilter(new WorkflowFilter({
                Id: workflowId
            }));

        let workflowModel = await this.WorkflowRepository.Find(spec);

        if(!workflowModel)
            return null;

        let updateSpec = await new UpdateWorkflowSpec()
            .SetValue({
                Template: body.Template
            });

        updateSpec.ByFilter(new WorkflowFilter({
            Id: workflowId
        }));

        await this.WorkflowRepository.Update(updateSpec);
    }
}