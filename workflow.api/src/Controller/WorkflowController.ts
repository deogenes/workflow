import { container, inject, injectable } from 'tsyringe';
import { IWorkflowRepository, WorkflowFilter } from '../../../workflow.domain';
import { WorkflowSpec } from '../../../workflow.infraestructure';


@injectable()
export class WorkflowController
{
    constructor(
        @inject("IWorkflowRepository")
        private WorkflowRepository: IWorkflowRepository
    )
    {
    }

    async Handle(workflowId: string)
    {
        let spec = await new WorkflowSpec()
            .ByFilter(new WorkflowFilter({
                Id: workflowId
            }));

        let workflowModel = await this.WorkflowRepository.Find(spec);

        if(!workflowModel)
            return null;

        return {
            Id: workflowModel.Id,
            Template: workflowModel.Template
        }
    }
}