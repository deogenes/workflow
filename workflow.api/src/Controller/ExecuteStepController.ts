import { container, inject, injectable } from 'tsyringe';
import { ExecuteFlowCommand } from "../../../workflow.application";
import { IMessageQueueProvider, MessageQueueEnvelop } from '../../../workflow.domain';


@injectable()
export class ExecuteStepController
{
    constructor(
        @inject("IMessageQueueProvider")
        private MessageQueueProvider: IMessageQueueProvider
    )
    {
    }

    /**
     * @param {Object} envelop
     */
    async Handle(envelop: any)
    {
        let command = new ExecuteFlowCommand({
            FlowId: envelop.Id,
            Data: envelop.Data || null,
            LastflowId: envelop.LastflowId,
            ScriptFile: `../ChildProcessCommand.js`
        });

        await this.MessageQueueProvider.Publish("executeWorker", new MessageQueueEnvelop({
            Content: command
        }));

        //let response = await this.ExecuteFlowCommandHandle.Handle(new ExecuteFlowCommand({
        //    FlowId: envelop.Id,
        //    Data: envelop.Data || null,
        //    LastflowId: envelop.LastflowId,
        //    ScriptFile: `${__dirname}/../ChildProcessCommand.js`
        //}));

        return true;
    }
}