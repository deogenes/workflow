import { inject, injectable } from 'tsyringe';
import { IFlowRepository } from '../../../workflow.domain';
import { DeleteFlowSpec, FlowSpec } from '../../../workflow.infraestructure';
import { FlowViewModel } from '../ViewModel/FlowViewModel';

@injectable()
export class DeleteFlowController
{
    constructor(
        @inject("IFlowRepository")
        private FlowRepository: IFlowRepository
    )
    {
    }

    async Handle(id: string)
    {
        let spec = new FlowSpec();
        spec.ByFilter({
            Id: id
        });

        let model = await this.FlowRepository.Find(spec);

        if(!model)
            return null;

        let deleteSpec = new DeleteFlowSpec();
        await this.FlowRepository.Delete(await deleteSpec.ById(id));

        return new FlowViewModel({
            Id: model.Id,
            QualifiedName: model.QualifiedName
        })
    }
}