import { inject, injectable } from 'tsyringe';
import { IFlowRepository } from '../../../workflow.domain';
import { CreateFlowSpec, CreateFlowSpecificationData } from '../../../workflow.infraestructure';

@injectable()
export class CreateFlowController
{
    constructor(
        @inject("IFlowRepository")
        private FlowRepository: IFlowRepository
    )
    {
    }

    async Handle(envelop: CreateFlowSpecificationData)
    {
        let spec = new CreateFlowSpec();
        spec.SetValue(envelop);

        await this.FlowRepository.Create(spec);
    }
}