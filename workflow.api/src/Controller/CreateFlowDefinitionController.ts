import { inject, injectable } from 'tsyringe';
import { IFlowDefinitionRepository, UpdateFlowDefinitionFilter } from '../../../workflow.domain';
import { CreateFlowDefinitionSpec, CreateFlowDefinitionSpecificationData, UpdateFlowDefinitionSpec } from '../../../workflow.infraestructure';

@injectable()
export class CreateFlowDefinitionController
{
    constructor(
        @inject("IFlowDefinitionRepository")
        private FlowDefinitionRepository: IFlowDefinitionRepository
    )
    {
    }

    async Handle(envelop: CreateFlowDefinitionSpecificationData)
    {
        let spec = new CreateFlowDefinitionSpec();
        spec.SetValue(envelop);

        await this.FlowDefinitionRepository.Create(spec);
    }
}