

export class FlowInfoViewModel
{
    public Id?: string;
    public Active? : boolean;
    public Pid?: string;
    public Memory?: string;

    constructor(args?: Partial<FlowInfoViewModel>)
    {
        Object.assign(this, args);
    }
}