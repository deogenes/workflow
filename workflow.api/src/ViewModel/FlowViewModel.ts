

export class FlowViewModel
{
    public Id?: string;
    public QualifiedName?: string

    constructor(args?: Partial<FlowViewModel>)
    {
        Object.assign(this, args);
    }
}