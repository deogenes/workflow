import "reflect-metadata";
import { container } from "tsyringe";
import './Configuration/IoC';

import { FlowSpec, UpdateFlowSpec } from "../../workflow.infraestructure";
import { FlowOutputModel, IFlowRepository, FlowFilter, FlowInputModel, IFlowModel } from "../../workflow.domain";

const axios = require('axios');

console.log(`PROCESSO FILHO #${process.pid}`)

let information = async (arg) => {

  const used = (process.memoryUsage().arrayBuffers / 1024);

  let data = {
    pid: process.pid,
    memoryUsage: Math.round(used * 100) / 100
  }

  process.send({
    event: "information",
    ...data
  });
};

let execute = async (arg) => {

  /** @type {IFlowModel<any, any,any>} flowInstance */
  var flowInstance = null;
  var success = true;

  /** @type { FlowOutputModel<any> } response */
  var response = null;
  
  try
  {
    /** @type { IFlowRepository } flowInstance */
    let repository = container.resolve("IFlowRepository");
    let spec = new FlowSpec();

    spec
      .AddNavigation({ Definition: { include: { NextFlows: true } } })
      .ByFilter(new FlowFilter({
        Id: arg.Id
      })
    );

    let flowModel = await repository.Find(spec);

    if(!flowModel)
      throw new Error(`Flow ${arg.id} não encontrado em nenhum modelo de workflow`);
    
    flowInstance = container.resolve(flowModel.QualifiedName);

    flowInstance.Init(flowModel);

    flowInstance.SetInput(new FlowInputModel({
      Id: arg.Id,
      LastFlowId: arg.LastFlowId,
      Data: arg.Data
    }));

    response = await flowInstance.GetOutput();

    console.log(`#${process.pid} Resposta do componente ${flowInstance.QualifiedName}`, response);
    //console.log(`#${process.pid} Resposta do componente para proximos nós`, await flowInstance.GetNext());
  }
  catch(error)
  {
    success = false;
  }
  finally
  {
    //Salvar status
    /** @type { IFlowRepository } flowInstance */
    let repository = container.resolve("IFlowRepository");

    let spec = await new UpdateFlowSpec();
    spec.ByFilter({
      Id: flowInstance.Id
    });

    spec.SetValue({
      Tries: ++flowInstance.Tries,
      Success: success,
      Executed: true
    });

    await repository.Update(spec);

    for(let nextFlowId of await flowInstance.GetNext())
    {
      //Mandar executar via API /execute
      console.log("Chamada para proximo nó (Fila ainda não implementada execute manualmente)", JSON.stringify({
        Id: nextFlowId,
        LastflowId: flowInstance.Id,
        Data: response.Data
      }));
    }

    process.exit(0);
  }
}

const events = {
  execute,
  information
}

process.on('message', (arg) => {
  events[arg.event](arg)
});