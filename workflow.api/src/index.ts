import './Configuration/IoC';
import "reflect-metadata";
import { container } from "tsyringe";
const numCPUs = require("os").cpus().length;
const http = require("http");
import { ExecuteStepController } from "./Controller/ExecuteStepController";
import { WorkflowSequencesController } from "./Controller/WorkflowSequencesController";
import { GetFlowProcessInfoCommandHandle, GetFlowsByFilterCommandHandle } from '../../workflow.application';
import { SocketIO, Pool, FlowRepository, createAdapter  } from '../../workflow.infraestructure';
import { UpdateFlowDefinitionController } from './Controller/UpdateFlowDefinitionController';
import { WorkflowController } from './Controller/WorkflowController';
import { UpdateWorkflowController } from './Controller/UpdateWorkflowController';
import { CreateFlowController } from './Controller/CreateFlowController';
import { CreateFlowDefinitionController } from './Controller/CreateFlowDefinitionController';
import { FlowController } from './Controller/FlowController';
import { DeleteFlowController } from './Controller/DeleteFlowController';
  
  console.log(`${process.pid} started`);

  const express = require('express');
  const app = express();
  var cors = require('cors')
  const bodyParser = require('body-parser')
  app.use(bodyParser.json())
  app.use(cors())

  var server = http.createServer(app);
  var io = SocketIO(server);

  const pool = new Pool({
    user: "postgres",
    host: "localhost",
    database: "postgres",
    password: "postgres",
    port: 5432,
  });
  
  pool.query(`
    CREATE TABLE IF NOT EXISTS socket_io_attachments (
        id          bigserial UNIQUE,
        created_at  timestamptz DEFAULT NOW(),
        payload     bytea
    );
  `);

  io.adapter(createAdapter(pool));

  app.post('/flow/', async(req: any, res: any) => res.send( await container.resolve<CreateFlowController>('CreateFlowController').Handle(req.body) ));
  app.post('/flow/definition/', async(req: any, res: any) => res.send( await container.resolve<CreateFlowDefinitionController>('CreateFlowDefinitionController').Handle(req.body) ));
  app.put('/flow/:flowId/definition/', async(req: any, res: any) => res.send( await container.resolve<UpdateFlowDefinitionController>('UpdateFlowDefinitionController').Handle(req.body, req.params.flowId) ));
  app.post('/execute/', async(req: any, res: any) => res.send( await container.resolve<ExecuteStepController>('ExecuteStepController').Handle(req.body) ));
  app.get('/workflow/:workflowId/sequence/:sequenceId/', async (req: any, res: any) => res.send( await container.resolve<WorkflowSequencesController>('WorkflowSequencesController').Handle(io, req.params.workflowId, req.params.sequenceId) ) );
  app.get('/flow/:flowId', async (req: any, res: any) => { let model =await container.resolve<FlowController>('FlowController').Handle(req.params.flowId); model == null ? res.sendStatus(404) : res.send(model); } );
  app.delete('/flow/:flowId', async (req: any, res: any) => { await container.resolve<DeleteFlowController>('DeleteFlowController').Handle(req.params.flowId); res.end(); } );
  app.get('/workflow/:workflowId', async (req: any, res: any) => res.send( await container.resolve<WorkflowController>('WorkflowController').Handle(req.params.workflowId) ) );
  app.put('/workflow/:workflowId', async (req: any, res: any) =>  { await container.resolve<UpdateWorkflowController>('UpdateWorkflowController').Handle(req.params.workflowId, { Template: req.body.Template }); res.end(); }); 

  io.on("GetFlowsByFilterCommand", async (arg: any, callback: any) => {
    let command = new GetFlowsByFilterCommandHandle(new FlowRepository);
    let response = await command.Handle(arg);
  
    callback(response);
  });

  io.on("GetFlowProcessInfoCommandHandle", async (arg: any, callback: any) => {
    console.log('Aqui api');

    let command = await container.resolve<GetFlowProcessInfoCommandHandle>("GetFlowProcessInfoCommandHandle");
    let response = await command.Handle(arg[0]);
  
    callback(response);
  });

  io.on("GetFlowProcessInfoCommandHandle", async (arg: any, callback: any) => {
    let command = await container.resolve<GetFlowProcessInfoCommandHandle>("GetFlowProcessInfoCommandHandle");
    let response = await command.Handle(arg[0]);
  
    callback(response);
  });

  io.on('connection', function(socket: any) {
  });

  server.listen(3000);