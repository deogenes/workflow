import { injectable } from "tsyringe";
import { FlowModel, FlowOutputModel, IFlowModel } from "../../../../workflow.domain";
import vm from 'node:vm';

export class ScriptFlowDefinitionModel
{
    Script?: string;
}

/**
 * Um componente que roda qualquer coisa
 */
@injectable()
export class ScriptFlowModel extends FlowModel<any, any, ScriptFlowDefinitionModel>
{
    constructor(
        args?: Partial<IFlowModel<any, any, ScriptFlowDefinitionModel>>
    )
    {
        super(args);
    }

    async GetOutput(): Promise<FlowOutputModel<any>> {

        //await new Promise((resolve, reject) => setTimeout(resolve, 500));

        let context = vm.createContext({
            $input: this.Input,
            output: undefined
        });
        
        const script = new vm.Script(this.Definition?.Properties?.Script as string);
        script.runInContext(context);

        return new FlowOutputModel({
            Data: context.output
        });
    }

    async GetNext(): Promise<Array<string>> {
        return this.Definition?.NextFlow as string[];
    }
}