import { injectable } from "tsyringe";
import { FlowModel, FlowOutputModel, IFlowModel, IFlowRepository } from "../../../../workflow.domain";

/**
 * Um componente que representa uma saída
 */
@injectable()
export class ConsoleLogFlowModel extends FlowModel<string, string, null>
{
    constructor(
        args?: Partial<IFlowModel<string, string, null>>
    )
    {
        super(args);
    }

    async GetOutput(): Promise<FlowOutputModel<string>> {

        await new Promise((resolve, reject) => { setTimeout(resolve, 3000) });

        let novaSaida = `Nova saida... ${this.Input?.Data}`;

        return new FlowOutputModel({
            Data: novaSaida
        });
    }

    async GetNext(): Promise<Array<string>> {
        return this.Definition?.NextFlow as string[];
    }
}