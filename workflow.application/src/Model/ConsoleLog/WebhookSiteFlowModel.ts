import { injectable } from "tsyringe";
import { FlowModel, FlowOutputModel, IFlowModel } from "../../../../workflow.domain";

enum WebhookSiteFlowMethod
{
    POST = "post",
    GET = "get"
}

export class WebhookSiteFlowDefinitionModel
{
    Method?: WebhookSiteFlowMethod
    Url?: string
}

export class WebhookSiteFlowInputModel
{
    Body?: string
}

export class WebhookSiteFlowOutputModel
{
    constructor(arg: Partial<WebhookSiteFlowOutputModel>)
    {
        Object.assign(this, arg);
    }

    StatusCode?: number
    Body?: string
}

/**
 * Envia um webhook ao Webhook.site
 */
@injectable()
export class WebhookSiteFlowModel extends FlowModel<WebhookSiteFlowInputModel, WebhookSiteFlowOutputModel, WebhookSiteFlowDefinitionModel>
{
    constructor(
        args?: Partial<IFlowModel<WebhookSiteFlowInputModel, WebhookSiteFlowOutputModel, WebhookSiteFlowDefinitionModel>>
    )
    {
        super(args);
    }

    async GetOutput(): Promise<FlowOutputModel<WebhookSiteFlowOutputModel>>
    {
        let url = this.Definition?.Properties?.Url;
        let method = this.Definition?.Properties?.Method as string;
        let body = this.Input?.Data?.Body;

        let response = await fetch(url as string, {
            body: body,
            method: method
        });

        let output = new WebhookSiteFlowOutputModel({
            Body: await response.text(),
            StatusCode: response.status
        });

        return new FlowOutputModel<WebhookSiteFlowOutputModel>({
            Data: output
        });
    }

    async GetNext(): Promise<Array<string>> {
        return this.Definition?.NextFlow as string[];
    }
}