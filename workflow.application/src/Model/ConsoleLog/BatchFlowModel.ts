import { FlowModel, FlowOutputModel } from "../../../../workflow.domain";

export class BatchFlowDefinitionModel
{
    CurrentBatch: number = 1;
    BatchSize?: number;
    NextFlowsDone?: Array<string>;
}

export class BatchFlowInputModel
{
    Items?: Array<any>
}

export class BatchFlowModel extends FlowModel<BatchFlowInputModel, any[], BatchFlowDefinitionModel>
{
    GetNext(): Promise<string[]> {

        if(this.Definition?.Properties?.BatchSize == this.Definition?.Properties?.CurrentBatch)
            return Promise.resolve(this.Definition?.NextFlow) as Promise<string[]>;
        
        return Promise.resolve(this.Definition?.Properties?.NextFlowsDone) as Promise<string[]>;
    }

    async GetOutput(): Promise<FlowOutputModel<any[]>> {
        //let flowIndex = this.Definition?.NextFlow?.indexOf(this.Input?.LastFlowId as string) as number;
        //let isInLooping = flowIndex > 0;

        //if(isInLooping)
            //Busca do DB os dados de entrada
        //else
            //Salva no DB os dados de entrada

        await new Promise((resolve, reject) => { setTimeout(resolve, 20000) });

        let bathSize = this.Definition?.Properties?.BatchSize as number;
        let currentBatch = this.Definition?.Properties?.CurrentBatch as number;

        let nextIndex = currentBatch * bathSize;
        let currentIndex = nextIndex / bathSize;
        
        let nextItems = this.Input?.Data?.Items?.slice(1 - currentIndex, nextIndex);

        let outputFlow = new FlowOutputModel({
            Data: nextItems
        });

        return Promise.resolve(outputFlow);
    }

}

