import { IBroadCastRPCProvider, IBroadCastRPCProviderFactory } from "../../../workflow.domain";
import { Server } from "socket.io";
import { IOBroadCastRPCProvider } from "../../../workflow.infraestructure";

export class IOBroadCastRPCProviderFactory implements IBroadCastRPCProviderFactory
{
    constructor(
        private io: Server
    )
    {
    }

    Create(): IBroadCastRPCProvider
    {
        let provider = new IOBroadCastRPCProvider(this.io);
        return provider;
    }
}