import { container, inject, injectable } from "tsyringe";
import { ChildProcess } from "child_process";
import ProcessMap from "../ChildProcessMap";

export class GetFlowProcessInfoCommandHandleResponse
{
    public constructor(args?: Partial<GetFlowProcessInfoCommandHandleResponse>)
    {
        Object.assign(this, args);
    }

    public MemoryUsage?: string;
    public PID?: string;
}

@injectable()
export class GetFlowProcessInfoCommandHandle
{
    async Handle(flowId: string) : Promise<GetFlowProcessInfoCommandHandleResponse | null>
    {
        let processMap = ProcessMap.Map;

        return new Promise<GetFlowProcessInfoCommandHandleResponse | null>((resolve, reject) => {
            try
            {
                let active = processMap.has(flowId);

                if(!active)
                    return resolve(null);
        
                let process = processMap.get(flowId) as ChildProcess;
    
                var processInformation = new GetFlowProcessInfoCommandHandleResponse();
        
                //var maxTimeout = setTimeout(() => reject(processInformation), 15000);
        
                process.send({
                    event: "information"
                });
    
                let listener = (args : any) => {
                    if(args.event != 'information')
                        return;
    
                    //clearInterval(maxTimeout);
                    process.removeListener("message", listener);
    
                    processInformation.MemoryUsage = args.memoryUsage;
                    processInformation.PID = args.pid;
    
                    resolve(processInformation);
                }
    
                process.on("message", listener);
            }
            catch(error)
            {
                resolve(null);
            }
        });
    }
}