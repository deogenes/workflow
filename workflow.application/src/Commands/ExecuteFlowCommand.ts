import { ChildProcess, fork } from "child_process";
import ProcessMap from "../ChildProcessMap";
import { inject, injectable } from "tsyringe";
import { FlowSpec } from "../../../workflow.infraestructure";
import { IFlowRepository } from "../../../workflow.domain";

export class ExecuteFlowCommand
{
    public constructor(args?: Partial<ExecuteFlowCommand>)
    {
        Object.assign(this, args);
    }

    public ScriptFile?: string;
    public FlowId: string = '';
    public LastflowId?: string;
    public Data?: any;
}

type ChildMessage = {
    event: string,
    Id?: string,
    LastFlowId?: string
    Data?: string
};

export class ExecuteFlowCommandHandleResponse
{
    constructor(args: Partial<ExecuteFlowCommandHandleResponse>)
    {
        Object.assign(this, args);
    }

    pid?: number
}

@injectable()
export class ExecuteFlowCommandHandle
{
    constructor(
        @inject("IFlowRepository")
        private FlowRepository: IFlowRepository
    )
    {
    }
    
    async Handle(command: ExecuteFlowCommand) : Promise<ExecuteFlowCommandHandleResponse>
    {
        let id = command.FlowId as string;
        let processMap = ProcessMap.Map;

        try
        {
            if(!id)
                throw new Error(`Id not specified`);

            let spec = new FlowSpec();
            spec.ByFilter({
                Id: id
            });

            let flowModel = await this.FlowRepository.Find(spec);

            if(!flowModel)
                throw new Error(`Flow '${id}' não encontrado em nenhum modelo de workflow`);

            if(processMap.has(id))
            {
                let process = processMap.get(id);

                return Promise.resolve(new ExecuteFlowCommandHandleResponse({
                    pid: process?.pid
                }));
            }

            await this.ExecuteProcess(command);
            
            return new ExecuteFlowCommandHandleResponse({});
        }
        catch (error: any)
        {
            let errorEx = error as Error;

            console.log("Falha", errorEx);
            processMap.delete(id);

            throw errorEx;
        }
    }

    private ExecuteProcess(command: ExecuteFlowCommand) : Promise<void>
    {
        return new Promise( (resolve, reject) => {
            let processMap = ProcessMap.Map;

            processMap.set(command.FlowId, fork(command.ScriptFile as string));
    
            let child = processMap.get(command.FlowId) as ChildProcess;
    
            child.on('exit', (code, signal) => {
                console.log(`Processo #${child.pid} finalizado codigo ${code} id: ${command.FlowId}`);
                processMap.delete(command.FlowId);

                if(code == 0)
                    return resolve();

                reject();
            });
    
            let message: ChildMessage = {
                event: 'execute',
                Id: command.FlowId,
                Data: command.Data,
                LastFlowId: command.LastflowId
            }
    
            child.send(message);
        } );
    }
}