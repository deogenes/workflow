import { inject, injectable } from "tsyringe";
import { FlowFilter, IBroadCastRPCProvider, IFlowModel, IFlowRepository } from "../../../workflow.domain";
import { FlowSpec } from "../../../workflow.infraestructure";
import { GetFlowProcessInfoCommandHandle, GetFlowProcessInfoCommandHandleResponse } from './GetFlowProcessInfoCommand'

export class GetSequenceInfoCommand
{
    public constructor(args?: Partial<GetSequenceInfoCommand>)
    {
        Object.assign(this, args);
    }

    WorkflowId?: string;
    SequenceId?: string;
}

export class GetSequenceInfoCommandHandleResponse
{
    public Id?: string;
    public Active? : boolean;
    public Pid?: string;
    public Memory?: string;

    constructor(args?: Partial<GetSequenceInfoCommandHandleResponse>)
    {
        Object.assign(this, args);
    }
}

@injectable()
export class GetSequenceInfoCommandHandle
{
    constructor(
        @inject("IFlowRepository")
        private FlowRepository: IFlowRepository,
        @inject("GetFlowProcessInfoCommandHandle")
        private GetFlowProcessInfoCommandHandle: GetFlowProcessInfoCommandHandle
    )
    {
    }
    
    async Handle(
        broadcastRPC: IBroadCastRPCProvider,
        command: GetSequenceInfoCommand
    ) : Promise<GetSequenceInfoCommandHandleResponse[]>
    {
        let spec = new FlowSpec();
        spec.ByFilter(new FlowFilter({
            WorkflowId: command.WorkflowId,
            Sequence: command.SequenceId
        }));

        //Buscando fluxos registrados no DB
        let fluxos = await this.FlowRepository.Where(spec);

        let flowInfoCollection = new Array<GetSequenceInfoCommandHandleResponse>();

        for(let fluxo of fluxos as IFlowModel<any, any, any>[] )
        {
            let flowInfo = new GetSequenceInfoCommandHandleResponse({Id: fluxo.Id});

            //Processo em outros servidores
            let _response = await broadcastRPC.Call<Array<any>>("GetFlowProcessInfoCommandHandle", fluxo.Id);

            let response = _response[0];
            console.log('Resposta remota', response);

            if(response != null)
            {
                let information : GetFlowProcessInfoCommandHandleResponse = response;

                flowInfo.Active = true;
                flowInfo.Memory = information.MemoryUsage;
                flowInfo.Pid = information.PID;
            }

            flowInfoCollection.push(flowInfo);
        }

        return flowInfoCollection;
    }
}