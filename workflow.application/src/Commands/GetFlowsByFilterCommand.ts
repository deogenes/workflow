import { FlowFilter, IFlowRepository } from "../../../workflow.domain";
import { FlowSpec } from "../../../workflow.infraestructure";

export class GetFlowsByFilterCommand
{
    public constructor(args?: Partial<GetFlowsByFilterCommand>)
    {
        Object.assign(this, args);
    }

    public WorkflowId?: string;
    public Sequence?: string;
}

export class GetFlowsByFilterCommandResponse
{
    public constructor(args?: Partial<GetFlowsByFilterCommandResponse>)
    {
        Object.assign(this, args);
    }

    public Id?: string;
}

export class GetFlowsByFilterCommandHandle
{
    FlowRepository: IFlowRepository;

    constructor(
       flowRepository: IFlowRepository
    )
    {
        this.FlowRepository = flowRepository;
    }

    async Handle(command: GetFlowsByFilterCommand) : Promise<Array<GetFlowsByFilterCommandResponse>>
    {
        let spec = new FlowSpec();

        spec.ByFilter(new FlowFilter({
            WorkflowId: command.WorkflowId,
            Sequence: command.Sequence
        }));

        let response = new Array<GetFlowsByFilterCommandResponse>();

        let flows = await this.FlowRepository.Where(spec);
        
        for(let flow of flows)
        {
            response.push(new GetFlowsByFilterCommandResponse({
                Id: flow.Id
            }));
        }

        return response;
    }
}