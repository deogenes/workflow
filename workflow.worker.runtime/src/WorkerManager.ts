import "reflect-metadata";
import { container, inject } from "tsyringe";
import './Configuration/IoC'
import './io';
import { IBroadCastRPCProvider, IMessageQueue, IMessageQueueProvider, MessageQueueEnvelop } from "../../workflow.domain";
import { ExecuteFlowCommand, GetFlowProcessInfoCommandHandleResponse, ProcessMap } from "../../workflow.application";
import { RabbitMessageQueueProvider } from "../../workflow.infraestructure";

export class WorkerManager
{
  public constructor(
    private BroadCastRPCProvider: IBroadCastRPCProvider,
    private Connection: IMessageQueueProvider
  )
  {
  }

  public async Handle(message: IMessageQueue)
  {
    try
    {
      let content = await message.Content<string>();
      let object = JSON.parse(content);
      let command = object as ExecuteFlowCommand;
  
      let running = ProcessMap.Map.has(command.FlowId);
  
      if(running || await this.IsRunning(command.FlowId))
      {
        console.log('Executando');
        return message.Reject();
      }
  
      if(running)
        ProcessMap.Map.delete(command.FlowId);

      console.log('Publish..')
  
      await this.Connection.Publish("executeWorker", new MessageQueueEnvelop({
        Content: command
      }));

      console.log('Confirm');

      await message.Confirm();

      console.log('ok Confirm');
    }
    catch (error)
    {
      console.log('Errox', (error as Error).message)
      await message.Reject();
    }
  }

  private async IsRunning(flowId: string) : Promise<boolean>
  {
    let rpcResponse = (await this.BroadCastRPCProvider.Call<Array<any>>("GetFlowProcessInfoCommandHandle", flowId)).find(x => x != undefined);
    let response = rpcResponse as GetFlowProcessInfoCommandHandleResponse | null;
    
    return response != undefined;
  }
}

let connection = new RabbitMessageQueueProvider("guest", "guest", "localhost", 5672);

let workerMager = new WorkerManager(
  container.resolve<IBroadCastRPCProvider>("IBroadCastRPCProvider"),
  connection
);

( async () => {
  await new Promise( ( resolve, reject ) => setTimeout(resolve, 3000));

  connection.Consume("executeManager", (msg: IMessageQueue) => workerMager.Handle(msg));
} )()