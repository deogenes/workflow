import { inject, injectable } from "tsyringe";
import { IBroadCastRPCProvider, IMessageQueue } from "../../../workflow.domain";
import { ExecuteFlowCommand, GetFlowProcessInfoCommandHandleResponse, ExecuteFlowCommandHandle, GetFlowProcessInfoCommandHandle } from "../../../workflow.application";

@injectable()
export class ExecuteService
{
    constructor(
        @inject("ExecuteFlowCommandHandle")
        private ExecuteFlowCommandHandle: ExecuteFlowCommandHandle
    )
    {
    }

    async Handle(message: IMessageQueue) : Promise<void>
    {
        try
        {
            let content = await message.Content<string>();
            let object = JSON.parse(content);
        
            let command = object as ExecuteFlowCommand;
    
            command.ScriptFile = `${__dirname}/${command.ScriptFile}`;
            await this.ExecuteFlowCommandHandle.Handle(command);

            await message.Confirm();
            console.log('Done');
        }
        catch(error)
        {
            await message.Reject();
        }

        return Promise.resolve();
    }
}