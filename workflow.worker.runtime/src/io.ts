import './Configuration/IoC';
import "reflect-metadata";
const http = require("http");
import { SocketIO, Pool, createAdapter  } from '../../workflow.infraestructure';
import { container } from 'tsyringe';
import { GetFlowProcessInfoCommandHandle } from '../../workflow.application/src';

  console.log(`${process.pid} started`);

  const express = require('express');
  const app = express();
  var cors = require('cors')
  const bodyParser = require('body-parser')
  app.use(bodyParser.json())
  app.use(cors())

  var server = http.createServer(app);
  var io = SocketIO(server);

  const pool = new Pool({
    user: "postgres",
    host: "localhost",
    database: "postgres",
    password: "postgres",
    port: 5432,
  });
  
  pool.query(`
    CREATE TABLE IF NOT EXISTS socket_io_attachments (
        id          bigserial UNIQUE,
        created_at  timestamptz DEFAULT NOW(),
        payload     bytea
    );
  `);

  io.adapter(createAdapter(pool, {
    requestsTimeout: 50000
  }));

  io.on("GetFlowProcessInfoCommandHandle", async (arg: any, callback: any) => {
    let command = await container.resolve<GetFlowProcessInfoCommandHandle>("GetFlowProcessInfoCommandHandle");
    let response = await command.Handle(arg[0]);

    console.log('remoto', response);
  
    callback(response);
  });

  io.on('connection', function(socket: any) {
  });

  server.listen(3003);

  globalThis.io = io;