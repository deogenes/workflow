import "reflect-metadata";
import { container } from "tsyringe";
import { ExecuteService } from "./Service/ExecuteService";
import './Configuration/IoC'
import './io';
import { IMessageQueue, IMessageQueueProvider } from "../../workflow.domain";

class Bootstrap
{
  public static async Main()
  {
    await new Promise( (resolve, reject) => setTimeout(resolve, 3000) )

    let connection = container.resolve<IMessageQueueProvider>("IMessageQueueProvider");
    
    await connection.Consume("executeWorker", async (msg: IMessageQueue) => await container.resolve<ExecuteService>("ExecuteService").Handle(msg))
    
    /*await connection.Publish("executeQueue", new MessageQueueEnvelop({
      Content: { time: Date.now() }
    }));*/
  }
}

Bootstrap.Main();