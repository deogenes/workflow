import "reflect-metadata";
import { container } from "tsyringe";
import './Configuration/IoC';
import { ExecuteFlowCommand } from "../../workflow.application";

import { FlowSpec, UpdateFlowSpec } from "../../workflow.infraestructure";
import { FlowOutputModel, IFlowRepository, FlowFilter, FlowInputModel, IFlowModel, IMessageQueueProvider, MessageQueueEnvelop, UpdateFlowFilter } from "../../workflow.domain";

console.log(`PROCESSO FILHO #${process.pid}`)

let information = async (arg: any) => {

  let send = process.send as (
    message: any,
    sendHandle?: any,
    options?: {
        swallowErrors?: boolean | undefined;
    },
    callback?: (error: Error | null) => void,
  ) => boolean

  const used = (process.memoryUsage().arrayBuffers / 1024);

  let data = {
    pid: process.pid,
    memoryUsage: Math.round(used * 100) / 100
  }

  send({
    event: "information",
    ...data
  });
};

let execute = async (arg : any) => {

  let messageQueueProvider = container.resolve<IMessageQueueProvider>("IMessageQueueProvider");

  var flowInstance: IFlowModel<any, any,any> | undefined = undefined;

  let repository = container.resolve<IFlowRepository>("IFlowRepository");
  let spec = new FlowSpec();

  await spec
    .AddNavigation({ Definition: { include: { NextFlows: true } } })
    .ByFilter(new FlowFilter({
      Id: arg.Id
    })
  );

  let flowModel = await repository.Find(spec);

  if(!flowModel)
    throw new Error(`Flow ${arg.id} não encontrado em nenhum modelo de workflow`);
  
  flowInstance = container.resolve(flowModel.QualifiedName as string);

  flowInstance?.Init(flowModel);

  flowInstance?.SetInput(new FlowInputModel({
    Id: arg.Id,
    LastFlowId: arg.LastFlowId,
    Data: arg.Data
  }));

  let tries = (flowInstance?.Tries || 0) as number;

    //Salvar status
  let updateSpec = await new UpdateFlowSpec();

  await updateSpec.ByFilter(new UpdateFlowFilter({
    Id: flowModel.Id
  }))

  await updateSpec.SetValue({
    Tries: ++tries
  });

  await repository.Update(updateSpec);

  let success = false;
  let nextNodes = new Array<string>();
  let response: FlowOutputModel<any> | undefined;

  try
  {
    response = await flowInstance?.GetOutput() as FlowOutputModel<any>;
    nextNodes = await flowInstance?.GetNext() as string[];

    console.log(`#${process.pid} Resposta do componente ${flowInstance?.QualifiedName}`, response);
    
    for(let nextFlowId of nextNodes)
    {
      let command = new ExecuteFlowCommand({
        FlowId: nextFlowId,
        Data: response?.Data,
        LastflowId: flowInstance?.Id,
        ScriptFile: `../ChildProcessCommand.js`
      });
  
      await messageQueueProvider.Publish("executeWorker", new MessageQueueEnvelop({
        Content: command
      }));
    }

    success = true;

    await updateSpec.SetValue({
      Success: success
    });
  
    await repository.Update(updateSpec);
  }
  catch(error)
  {
    console.log(error);
  }

  process.exit(success ? 0 : 1);
}

const events : Record<string, (arg: Partial<{event: string}>) => any> = {
  'execute': execute,
  'information': information
};

process.on('message', async (arg: Partial<{event: string}>) => {
  let method = events[arg?.event as string];

  if(!method)
    process.exit(1);

  try
  {
    await method(arg);
  }
  catch(error)
  {
    console.log(error)
    process.exit(1);
  }
});