import "reflect-metadata";
import { registry } from "tsyringe";
import { FlowRepository, IOBroadCastRPCProvider, RabbitMessageQueueProvider, Server } from "../../../workflow.infraestructure";
import { BatchFlowModel, ConsoleLogFlowModel, ExecuteFlowCommandHandle, GetFlowProcessInfoCommandHandle, ScriptFlowModel, WebhookSiteFlowModel } from "../../../workflow.application";
import { ExecuteService } from "../Service/ExecuteService";
import { IFlowRepository } from "../../../workflow.domain";

declare global {
  var io : Server;
  var rabbitMessageQueueProvider: RabbitMessageQueueProvider;
}

@registry([
  {     
    token: "IMessageQueueProvider", useFactory(dependencyContainer) {
      if(globalThis.rabbitMessageQueueProvider == undefined)
        global.rabbitMessageQueueProvider = new RabbitMessageQueueProvider("guest", "guest", "localhost", 5672)

      return global.rabbitMessageQueueProvider
    }
  },
  { token: "GetFlowProcessInfoCommandHandle", useClass: GetFlowProcessInfoCommandHandle },
  { token: "IFlowRepository", useClass: FlowRepository  },
  {
    token: "ExecuteService", useFactory(di) {
      return new ExecuteService(
        di.resolve<ExecuteFlowCommandHandle>("ExecuteFlowCommandHandle")
      )
    }
  },
  {
    token: 'IBroadCastRPCProvider', useFactory(di) {
      return new IOBroadCastRPCProvider(globalThis.io);
    }
  },
  {
    token: "ExecuteFlowCommandHandle", useFactory(di) {
      return new ExecuteFlowCommandHandle(di.resolve<IFlowRepository>("IFlowRepository"))
    }
  },
  {
    token: "WebhookSiteFlowModel", useFactory(container) {
      return new WebhookSiteFlowModel({})
    },
  },
  {
    token: "ConsoleLogFlowModel", useFactory(container) {
      return new ConsoleLogFlowModel({})
    }
  },
  {
    token: "BatchFlowModel", useFactory(container) {
      return new BatchFlowModel({})
    }
  },
  {
    token: "ScriptFlowModel", useFactory(container) {
      return new ScriptFlowModel({})
    }
  },
])

class MyRegistry {}