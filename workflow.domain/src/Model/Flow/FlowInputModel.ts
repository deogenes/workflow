export class FlowInputModel<T>
{
    constructor(
        args?: Partial<FlowInputModel<T>>
    )
    {
        Object.assign(this, args);
    }

    public Id?: string;
    public LastFlowId?: string;
    public Data?: T
}
