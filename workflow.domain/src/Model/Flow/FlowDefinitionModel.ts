export class FlowDefinitionModel<T>
{
    constructor(args?: Partial<FlowDefinitionModel<T>>)
    {
        Object.assign(this, args);
    }

    Id?: String
    FlowId?: String
    Properties?: T
    NextFlow: Array<string> = new Array<string>()
}
