export class FlowOutputModel<T>
{
    constructor(args?: Partial<FlowOutputModel<T>>)
    {
        Object.assign(this, args);
    }

    public Data?: T
}
