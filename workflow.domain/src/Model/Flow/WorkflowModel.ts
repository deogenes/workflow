
export class WorkflowModel
{
    public Id?: string;

    public Template?: string;

    public constructor(arg: Partial<WorkflowModel>)
    {
        Object.assign(this, arg);
    }
}