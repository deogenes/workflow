import { IFlowModel } from "../../Contract/IFlowModel";
import { FlowDefinitionModel } from "./FlowDefinitionModel";
import { FlowInputModel } from "./FlowInputModel";
import { FlowOutputModel } from "./FlowOutputModel";

export class FlowModel<InputT, OutputT, PropertiesT> implements IFlowModel<InputT, OutputT, PropertiesT>
{
    public Id?: string;
    public Tries?: Number;
    public Executed?: boolean;
    public Success?: boolean;
    public Sequence?: string;
    public WorkflowId?: string;
    public QualifiedName?: string;
    public Definition?: FlowDefinitionModel<PropertiesT>;
    public Input?: FlowInputModel<InputT>;
    
    constructor(args?: Partial<IFlowModel<InputT, OutputT, PropertiesT>>)
    {
        this.Init(args);
    }

    Init(args?: Partial<IFlowModel<InputT, OutputT, PropertiesT>>) : IFlowModel<InputT, OutputT, PropertiesT>
    {
        Object.assign(this, args);
        return this;
    }
    
    GetOutput(): Promise<FlowOutputModel<OutputT>> {
        throw new Error("Method not implemented.");
    }

    GetNext(): Promise<string[]> {
        throw new Error("Method not implemented.");
    }

    SetInput(args: FlowInputModel<InputT>): Promise<void> {
        this.Input = args;

        return Promise.resolve();
    }
}