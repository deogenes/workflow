
export class UpdateFlowDefinitionFilter
{
    public constructor(args?: Partial<UpdateFlowDefinitionFilter>)
    {
        Object.assign(this, args);
    }

    public Id?: string;
    public FlowId?: string;
}