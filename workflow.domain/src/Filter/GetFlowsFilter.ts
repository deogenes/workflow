
export class FlowFilter
{
    public constructor(args?: Partial<FlowFilter>)
    {
        Object.assign(this, args);
    }

    public Id?: string;
    public WorkflowId?: string;
    public Sequence?: string;
}