
export class FlowDefinitionFilter
{
    public constructor(args?: Partial<FlowDefinitionFilter>)
    {
        Object.assign(this, args);
    }

    public Id?: string;
}