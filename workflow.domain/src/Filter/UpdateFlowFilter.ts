
export class UpdateFlowFilter
{
    public constructor(args?: Partial<UpdateFlowFilter>)
    {
        Object.assign(this, args);
    }

    public Id?: string;
    public WorkflowId?: string;
}