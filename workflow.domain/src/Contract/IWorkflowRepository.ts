import { WorkflowModel } from "../Model/Flow/WorkflowModel";
import { IRepository } from "./IRepository";

export interface IWorkflowRepository extends IRepository<WorkflowModel>
{
    
}