import { IBroadCastRPCProvider } from "./IBroadCastRPCProvider";

export interface IBroadCastRPCProviderFactory
{
    Create(): IBroadCastRPCProvider
}