import { IFlowModel } from "./IFlowModel";
import { IRepository } from "./IRepository";
import { ISpec } from "./ISpec";

export interface IFlowRepository extends IRepository<IFlowModel<any, any, any>>
{
    Create(spec: ISpec<IFlowModel<any, any, any>>) : Promise<void>

    Delete(spec: ISpec<IFlowModel<any, any, any>>) : Promise<void>
}