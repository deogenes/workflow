import { FlowDefinitionModel } from "../Model/Flow/FlowDefinitionModel";
import { FlowInputModel } from "../Model/Flow/FlowInputModel";
import { FlowOutputModel } from "../Model/Flow/FlowOutputModel";

export interface IFlowModel<InputT, OutputT, PropertiesT>
{
    Id?: string;
    Tries?: Number;
    Executed?: boolean;
    Success?: boolean;
    Sequence?: string;
    WorkflowId?: string;
    QualifiedName?: string;
    Definition?: FlowDefinitionModel<PropertiesT>;
    
    Init(args?: Partial<IFlowModel<InputT, OutputT, PropertiesT>>) : IFlowModel<InputT, OutputT, PropertiesT>;
    SetInput(args: FlowInputModel<InputT>) : Promise<void>;
    GetOutput() : Promise<FlowOutputModel<OutputT>>;
    GetNext(): Promise<Array<string>>;
}