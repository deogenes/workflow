import { FlowDefinitionModel } from "../Model/Flow/FlowDefinitionModel";
import { IRepository } from "./IRepository";
import { ISpec } from "./ISpec";

export interface IFlowDefinitionRepository extends IRepository<FlowDefinitionModel<any>>
{
    Create(spec: ISpec<FlowDefinitionModel<any>>) : Promise<void>
}