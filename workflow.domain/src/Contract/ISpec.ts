
export interface ISpec<T>
{
    GetSpecification(): Promise<any>
}