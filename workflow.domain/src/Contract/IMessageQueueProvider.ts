
export interface IMessageQueue
{
  Content<T>() : Promise<T>;
  Reject() : Promise<void>;
  Confirm() : Promise<void>;
}

export class MessageQueueEnvelop
{
  constructor(arg: Partial<MessageQueueEnvelop>)
  {
    Object.assign(this, arg);
  }

  Content : any;
}

export interface IMessageQueueProvider
{
  Consume(subject: string, handler: (msg: IMessageQueue) => any) : Promise<void>
  
  Publish(subject: string, envelop: MessageQueueEnvelop) : Promise<void>
}
