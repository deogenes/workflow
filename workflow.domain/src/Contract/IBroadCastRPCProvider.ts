
export interface IBroadCastRPCProvider
{
    Call<T>(method: string, ...args: any): Promise<T>
}