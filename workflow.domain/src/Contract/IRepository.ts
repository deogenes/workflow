import { ISpec } from "./ISpec";

export interface IRepository<T>
{
    Where(spec: ISpec<T>) : Promise<Array<T>>

    Find(spec: ISpec<T>) : Promise<T | null>

    Update(spec: ISpec<T>) : Promise<void>
}